import java.util.HashMap;

public class Pizza {

	static HashMap<String, Integer> ingrediente = new HashMap<String, Integer>();
	double preco;
	int qtdIngrediente = 0;
	
	public void adicionarIngrediente(String ingrediente) {
		if(this.ingrediente.containsKey(ingrediente)) {
			int qtdIngrediente = Pizza.ingrediente.get(ingrediente);
			qtdIngrediente++;
			this.ingrediente.put(ingrediente, qtdIngrediente);
		}else {
			this.ingrediente.put(ingrediente, 1);
		}
		qtdIngrediente ++;
		
		//contabilizarIngrediente();
	}
	
	public static void contabilizarIngrediente() {
		for(String key : Pizza.ingrediente.keySet()) {
			System.out.println(Pizza.ingrediente.get(key) + " " + key);
		}
	}
	
	public int quantidadeIngrediente(Pizza pizza) {
		return pizza.qtdIngrediente;
	}
	
	public static int zerarIngredientes (){
		ingrediente.clear();
		return ingrediente.size();
	}

	public double getPreco() {
		if(quantidadeIngrediente(this) <= 2) 
			return 15;
		if(quantidadeIngrediente(this) <= 5)
			return 20;
		return 23;
	}

}

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PizzariaTeste {
	Pizza p1;
	CarrinhoDeCompras carrinho;

	@Before
	public void inicializadorPizza() {
		p1 = new Pizza();
		carrinho = new CarrinhoDeCompras();
		Pizza.zerarIngredientes();
	}

	@Test
	public void valorMenosDeTresIngrediente() {
		p1.adicionarIngrediente("Frango");
		p1.adicionarIngrediente("Catupiry");

		p1.quantidadeIngrediente(p1);

		assertEquals(15, p1.getPreco(), 0.001);
	}

	@Test
	public void valorDeTresACincoIngrediente() {
		p1.adicionarIngrediente("Queijo");
		p1.adicionarIngrediente("Calabresa");
		p1.adicionarIngrediente("Cebola");

		p1.quantidadeIngrediente(p1);

		assertEquals(20, p1.getPreco(), 0.001);
	}

	@Test
	public void valorMaisDeCincoIngrediente() {

		p1.adicionarIngrediente("Queijo");
		p1.adicionarIngrediente("Presunto");
		p1.adicionarIngrediente("Ovo");
		p1.adicionarIngrediente("Milho");
		p1.adicionarIngrediente("Ervilha");
		p1.adicionarIngrediente("Azeitona");

		p1.quantidadeIngrediente(p1);

		assertEquals(23, p1.getPreco(), 0.001);
	}

	@Test
	public void valorTotalPizza() {
		Pizza p2 = new Pizza();
		Pizza p3 = new Pizza();

		p1.adicionarIngrediente("Queijo");
		p1.adicionarIngrediente("Calabresa");
		p1.adicionarIngrediente("Cebola");

		p2.adicionarIngrediente("Queijo");
		p2.adicionarIngrediente("Presunto");
		p2.adicionarIngrediente("Ovo");
		p2.adicionarIngrediente("Milho");
		p2.adicionarIngrediente("Ervilha");
		p2.adicionarIngrediente("Azeitona");

		p3.adicionarIngrediente("Frango");
		p3.adicionarIngrediente("Catupiry");

		carrinho.retornarValor(p1);
		carrinho.retornarValor(p2);
		carrinho.retornarValor(p3);

		assertEquals(58, carrinho.getValorTotal(), 0.001);

	}

	@Test
	public void pizzaSemIngrediente() {
		assertEquals(0, carrinho.retornarValor(p1), 0.001);
	}
	
	@Test
	public void zerarIngrediente() {
		assertEquals(0, Pizza.zerarIngredientes());
	}
}
